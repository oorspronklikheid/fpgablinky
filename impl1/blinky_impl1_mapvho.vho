
-- VHDL netlist produced by program ldbanno, Version Diamond (64-bit) 3.10.0.111.2

-- ldbanno -n VHDL -o blinky_impl1_mapvho.vho -w -neg -gui -msgset C:/Users/password/Documents/Projects/fpga/promote.xml blinky_impl1_map.ncd 
-- Netlist created on Mon Apr 01 05:58:07 2019
-- Netlist written on Mon Apr 01 05:58:13 2019
-- Design is for device LFE5U-25F
-- Design is for package CABGA381
-- Design is for performance grade 6

-- entity sapiobuf
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity sapiobuf is
    port (I: in Std_logic; PAD: out Std_logic);

    ATTRIBUTE Vital_Level0 OF sapiobuf : ENTITY IS TRUE;

  end sapiobuf;

  architecture Structure of sapiobuf is
  begin
    INST1: OB
      port map (I=>I, O=>PAD);
  end Structure;

-- entity gnd
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity gnd is
    port (PWR0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF gnd : ENTITY IS TRUE;

  end gnd;

  architecture Structure of gnd is
  begin
    INST1: VLO
      port map (Z=>PWR0);
  end Structure;

-- entity LVDS_Red_0_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity LVDS_Red_0_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "LVDS_Red_0_B");

    port (LVDSRed0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF LVDS_Red_0_B : ENTITY IS TRUE;

  end LVDS_Red_0_B;

  architecture Structure of LVDS_Red_0_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal LVDSRed0_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component sapiobuf
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
    component gnd
      port (PWR0: out Std_logic);
    end component;
  begin
    LVDS_Red_pad_0: sapiobuf
      port map (I=>GNDI, PAD=>LVDSRed0_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (LVDSRed0_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    LVDSRed0 	<= LVDSRed0_out;


    END PROCESS;

  end Structure;

-- entity sapiobuf0001
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity sapiobuf0001 is
    port (Z: out Std_logic; PAD: in Std_logic);

    ATTRIBUTE Vital_Level0 OF sapiobuf0001 : ENTITY IS TRUE;

  end sapiobuf0001;

  architecture Structure of sapiobuf0001 is
  begin
    INST1: IBPD
      port map (I=>PAD, O=>Z);
  end Structure;

-- entity sys_clockB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity sys_clockB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "sys_clockB";

      tipd_sysclock  	: VitalDelayType01 := (0 ns, 0 ns);
      tpd_sysclock_PADDI	 : VitalDelayType01 := (0 ns, 0 ns);
      tperiod_sysclock 	: VitalDelayType := 0 ns;
      tpw_sysclock_posedge	: VitalDelayType := 0 ns;
      tpw_sysclock_negedge	: VitalDelayType := 0 ns);

    port (PADDI: out Std_logic; sysclock: in Std_logic);

    ATTRIBUTE Vital_Level0 OF sys_clockB : ENTITY IS TRUE;

  end sys_clockB;

  architecture Structure of sys_clockB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal PADDI_out 	: std_logic := 'X';
    signal sysclock_ipd 	: std_logic := 'X';

    component sapiobuf0001
      port (Z: out Std_logic; PAD: in Std_logic);
    end component;
  begin
    sys_clock_pad: sapiobuf0001
      port map (Z=>PADDI_out, PAD=>sysclock_ipd);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
      VitalWireDelay(sysclock_ipd, sysclock, tipd_sysclock);
    END BLOCK;

    VitalBehavior : PROCESS (PADDI_out, sysclock_ipd)
    VARIABLE PADDI_zd         	: std_logic := 'X';
    VARIABLE PADDI_GlitchData 	: VitalGlitchDataType;

    VARIABLE tviol_sysclock_sysclock          	: x01 := '0';
    VARIABLE periodcheckinfo_sysclock	: VitalPeriodDataType;

    BEGIN

    IF (TimingChecksOn) THEN
      VitalPeriodPulseCheck (
        TestSignal => sysclock_ipd,
        TestSignalName => "sysclock",
        Period => tperiod_sysclock,
        PulseWidthHigh => tpw_sysclock_posedge,
        PulseWidthLow => tpw_sysclock_negedge,
        PeriodData => periodcheckinfo_sysclock,
        Violation => tviol_sysclock_sysclock,
        MsgOn => MsgOn, XOn => XOn,
        HeaderMsg => InstancePath,
        CheckEnabled => TRUE,
        MsgSeverity => warning);

    END IF;

    PADDI_zd 	:= PADDI_out;

    VitalPathDelay01 (
      OutSignal => PADDI, OutSignalName => "PADDI", OutTemp => PADDI_zd,
      Paths      => (0 => (InputChangeTime => sysclock_ipd'last_event,
                           PathDelay => tpd_sysclock_PADDI,
                           PathCondition => TRUE)),
      GlitchData => PADDI_GlitchData,
      Mode       => vitaltransport, XOn => XOn, MsgOn => MsgOn);

    END PROCESS;

  end Structure;

-- entity sapiobuf0002
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity sapiobuf0002 is
    port (I: in Std_logic; PAD: out Std_logic);

    ATTRIBUTE Vital_Level0 OF sapiobuf0002 : ENTITY IS TRUE;

  end sapiobuf0002;

  architecture Structure of sapiobuf0002 is
  begin
    INST5: OB
      port map (I=>I, O=>PAD);
  end Structure;

-- entity vcc
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity vcc is
    port (PWR1: out Std_logic);

    ATTRIBUTE Vital_Level0 OF vcc : ENTITY IS TRUE;

  end vcc;

  architecture Structure of vcc is
  begin
    INST1: VHI
      port map (Z=>PWR1);
  end Structure;

-- entity PS2_enableB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity PS2_enableB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "PS2_enableB");

    port (PS2enable: out Std_logic);

    ATTRIBUTE Vital_Level0 OF PS2_enableB : ENTITY IS TRUE;

  end PS2_enableB;

  architecture Structure of PS2_enableB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal PS2enable_out 	: std_logic := 'X';

    signal VCCI: Std_logic;
    component sapiobuf0002
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
    component vcc
      port (PWR1: out Std_logic);
    end component;
  begin
    PS2_enable_pad: sapiobuf0002
      port map (I=>VCCI, PAD=>PS2enable_out);
    DRIVEVCC: vcc
      port map (PWR1=>VCCI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (PS2enable_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    PS2enable 	<= PS2enable_out;


    END PROCESS;

  end Structure;

-- entity mmc_mosiB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity mmc_mosiB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "mmc_mosiB");

    port (mmcmosi: out Std_logic);

    ATTRIBUTE Vital_Level0 OF mmc_mosiB : ENTITY IS TRUE;

  end mmc_mosiB;

  architecture Structure of mmc_mosiB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal mmcmosi_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0002
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    mmc_mosi_pad: sapiobuf0002
      port map (I=>GNDI, PAD=>mmcmosi_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (mmcmosi_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    mmcmosi 	<= mmcmosi_out;


    END PROCESS;

  end Structure;

-- entity mmc_clkB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity mmc_clkB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "mmc_clkB");

    port (mmcclk: out Std_logic);

    ATTRIBUTE Vital_Level0 OF mmc_clkB : ENTITY IS TRUE;

  end mmc_clkB;

  architecture Structure of mmc_clkB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal mmcclk_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0002
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    mmc_clk_pad: sapiobuf0002
      port map (I=>GNDI, PAD=>mmcclk_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (mmcclk_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    mmcclk 	<= mmcclk_out;


    END PROCESS;

  end Structure;

-- entity mmc_n_csB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity mmc_n_csB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "mmc_n_csB");

    port (mmcncs: out Std_logic);

    ATTRIBUTE Vital_Level0 OF mmc_n_csB : ENTITY IS TRUE;

  end mmc_n_csB;

  architecture Structure of mmc_n_csB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal mmcncs_out 	: std_logic := 'X';

    signal VCCI: Std_logic;
    component sapiobuf0002
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
    component vcc
      port (PWR1: out Std_logic);
    end component;
  begin
    mmc_n_cs_pad: sapiobuf0002
      port map (I=>VCCI, PAD=>mmcncs_out);
    DRIVEVCC: vcc
      port map (PWR1=>VCCI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (mmcncs_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    mmcncs 	<= mmcncs_out;


    END PROCESS;

  end Structure;

-- entity sapiobuf0003
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity sapiobuf0003 is
    port (I: in Std_logic; PAD: out Std_logic);

    ATTRIBUTE Vital_Level0 OF sapiobuf0003 : ENTITY IS TRUE;

  end sapiobuf0003;

  architecture Structure of sapiobuf0003 is
  begin
    INST5: OB
      port map (I=>I, O=>PAD);
  end Structure;

-- entity GPIO_2B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity GPIO_2B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "GPIO_2B";

      tipd_IOLDO  	: VitalDelayType01 := (0 ns, 0 ns);
      tpd_IOLDO_GPIO2	 : VitalDelayType01 := (0 ns, 0 ns));

    port (IOLDO: in Std_logic; GPIO2: out Std_logic);

    ATTRIBUTE Vital_Level0 OF GPIO_2B : ENTITY IS TRUE;

  end GPIO_2B;

  architecture Structure of GPIO_2B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal IOLDO_ipd 	: std_logic := 'X';
    signal GPIO2_out 	: std_logic := 'X';

    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    GPIO_2_pad: sapiobuf0003
      port map (I=>IOLDO_ipd, PAD=>GPIO2_out);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
      VitalWireDelay(IOLDO_ipd, IOLDO, tipd_IOLDO);
    END BLOCK;

    VitalBehavior : PROCESS (IOLDO_ipd, GPIO2_out)
    VARIABLE GPIO2_zd         	: std_logic := 'X';
    VARIABLE GPIO2_GlitchData 	: VitalGlitchDataType;


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    GPIO2_zd 	:= GPIO2_out;

    VitalPathDelay01 (
      OutSignal => GPIO2, OutSignalName => "GPIO2", OutTemp => GPIO2_zd,
      Paths      => (0 => (InputChangeTime => IOLDO_ipd'last_event,
                           PathDelay => tpd_IOLDO_GPIO2,
                           PathCondition => TRUE)),
      GlitchData => GPIO2_GlitchData,
      Mode       => vitaltransport, XOn => XOn, MsgOn => MsgOn);

    END PROCESS;

  end Structure;

-- entity mfflsre
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity mfflsre is
    port (D0: in Std_logic; SP: in Std_logic; CK: in Std_logic; 
          LSR: in Std_logic; Q: out Std_logic);

    ATTRIBUTE Vital_Level0 OF mfflsre : ENTITY IS TRUE;

  end mfflsre;

  architecture Structure of mfflsre is
  begin
    INST01: FD1P3DX
      generic map (GSR => "DISABLED")
      port map (D=>D0, SP=>SP, CK=>CK, CD=>LSR, Q=>Q);
  end Structure;

-- entity GPIO_2_MGIOL
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity GPIO_2_MGIOL is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "GPIO_2_MGIOL";

      tipd_TXDATA0  	: VitalDelayType01 := (0 ns, 0 ns);
      tipd_CLK  	: VitalDelayType01 := (0 ns, 0 ns);
      tpd_CLK_IOLDO	 : VitalDelayType01 := (0 ns, 0 ns);
      ticd_CLK	: VitalDelayType := 0 ns;
      tisd_TXDATA0_CLK	: VitalDelayType := 0 ns;
      tsetup_TXDATA0_CLK_noedge_posedge	: VitalDelayType := 0 ns;
      thold_TXDATA0_CLK_noedge_posedge	: VitalDelayType := 0 ns;
      tperiod_CLK 	: VitalDelayType := 0 ns;
      tpw_CLK_posedge	: VitalDelayType := 0 ns;
      tpw_CLK_negedge	: VitalDelayType := 0 ns);

    port (IOLDO: out Std_logic; TXDATA0: in Std_logic; CLK: in Std_logic);

    ATTRIBUTE Vital_Level0 OF GPIO_2_MGIOL : ENTITY IS TRUE;

  end GPIO_2_MGIOL;

  architecture Structure of GPIO_2_MGIOL is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal IOLDO_out 	: std_logic := 'X';
    signal TXDATA0_ipd 	: std_logic := 'X';
    signal TXDATA0_dly 	: std_logic := 'X';
    signal CLK_ipd 	: std_logic := 'X';
    signal CLK_dly 	: std_logic := 'X';

    signal VCCI: Std_logic;
    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component vcc
      port (PWR1: out Std_logic);
    end component;
    component mfflsre
      port (D0: in Std_logic; SP: in Std_logic; CK: in Std_logic; 
            LSR: in Std_logic; Q: out Std_logic);
    end component;
  begin
    user_module1_blink_LEDio: mfflsre
      port map (D0=>TXDATA0_dly, SP=>VCCI, CK=>CLK_dly, LSR=>GNDI, 
                Q=>IOLDO_out);
    DRIVEVCC: vcc
      port map (PWR1=>VCCI);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
      VitalWireDelay(TXDATA0_ipd, TXDATA0, tipd_TXDATA0);
      VitalWireDelay(CLK_ipd, CLK, tipd_CLK);
    END BLOCK;

    --  Setup and Hold DELAYs
    SignalDelay : BLOCK
    BEGIN
      VitalSignalDelay(TXDATA0_dly, TXDATA0_ipd, tisd_TXDATA0_CLK);
      VitalSignalDelay(CLK_dly, CLK_ipd, ticd_CLK);
    END BLOCK;

    VitalBehavior : PROCESS (IOLDO_out, TXDATA0_dly, CLK_dly)
    VARIABLE IOLDO_zd         	: std_logic := 'X';
    VARIABLE IOLDO_GlitchData 	: VitalGlitchDataType;

    VARIABLE tviol_TXDATA0_CLK       	: x01 := '0';
    VARIABLE TXDATA0_CLK_TimingDatash	: VitalTimingDataType;
    VARIABLE tviol_CLK_CLK          	: x01 := '0';
    VARIABLE periodcheckinfo_CLK	: VitalPeriodDataType;

    BEGIN

    IF (TimingChecksOn) THEN
      VitalSetupHoldCheck (
        TestSignal => TXDATA0_dly,
        TestSignalName => "TXDATA0",
        TestDelay => tisd_TXDATA0_CLK,
        RefSignal => CLK_dly,
        RefSignalName => "CLK",
        RefDelay => ticd_CLK,
        SetupHigh => tsetup_TXDATA0_CLK_noedge_posedge,
        SetupLow => tsetup_TXDATA0_CLK_noedge_posedge,
        HoldHigh => thold_TXDATA0_CLK_noedge_posedge,
        HoldLow => thold_TXDATA0_CLK_noedge_posedge,
        CheckEnabled => TRUE,
        RefTransition => '/',
        MsgOn => MsgOn, XOn => XOn,
        HeaderMsg => InstancePath,
        TimingData => TXDATA0_CLK_TimingDatash,
        Violation => tviol_TXDATA0_CLK,
        MsgSeverity => warning);
      VitalPeriodPulseCheck (
        TestSignal => CLK_ipd,
        TestSignalName => "CLK",
        Period => tperiod_CLK,
        PulseWidthHigh => tpw_CLK_posedge,
        PulseWidthLow => tpw_CLK_negedge,
        PeriodData => periodcheckinfo_CLK,
        Violation => tviol_CLK_CLK,
        MsgOn => MsgOn, XOn => XOn,
        HeaderMsg => InstancePath,
        CheckEnabled => TRUE,
        MsgSeverity => warning);

    END IF;

    IOLDO_zd 	:= IOLDO_out;

    VitalPathDelay01 (
      OutSignal => IOLDO, OutSignalName => "IOLDO", OutTemp => IOLDO_zd,
      Paths      => (0 => (InputChangeTime => CLK_dly'last_event,
                           PathDelay => tpd_CLK_IOLDO,
                           PathCondition => TRUE)),
      GlitchData => IOLDO_GlitchData,
      Mode       => ondetect, XOn => XOn, MsgOn => MsgOn);

    END PROCESS;

  end Structure;

-- entity Dram_DQMLB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_DQMLB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_DQMLB");

    port (DramDQML: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_DQMLB : ENTITY IS TRUE;

  end Dram_DQMLB;

  architecture Structure of Dram_DQMLB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramDQML_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_DQML_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramDQML_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramDQML_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramDQML 	<= DramDQML_out;


    END PROCESS;

  end Structure;

-- entity Dram_DQMHB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_DQMHB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_DQMHB");

    port (DramDQMH: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_DQMHB : ENTITY IS TRUE;

  end Dram_DQMHB;

  architecture Structure of Dram_DQMHB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramDQMH_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_DQMH_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramDQMH_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramDQMH_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramDQMH 	<= DramDQMH_out;


    END PROCESS;

  end Structure;

-- entity Dram_n_csB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_n_csB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_n_csB");

    port (Dramncs: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_n_csB : ENTITY IS TRUE;

  end Dram_n_csB;

  architecture Structure of Dram_n_csB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal Dramncs_out 	: std_logic := 'X';

    signal VCCI: Std_logic;
    component vcc
      port (PWR1: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_n_cs_pad: sapiobuf0003
      port map (I=>VCCI, PAD=>Dramncs_out);
    DRIVEVCC: vcc
      port map (PWR1=>VCCI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (Dramncs_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    Dramncs 	<= Dramncs_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_12_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_12_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_12_B");

    port (DramAddr12: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_12_B : ENTITY IS TRUE;

  end Dram_Addr_12_B;

  architecture Structure of Dram_Addr_12_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr12_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_12: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr12_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr12_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr12 	<= DramAddr12_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_11_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_11_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_11_B");

    port (DramAddr11: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_11_B : ENTITY IS TRUE;

  end Dram_Addr_11_B;

  architecture Structure of Dram_Addr_11_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr11_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_11: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr11_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr11_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr11 	<= DramAddr11_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_10_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_10_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_10_B");

    port (DramAddr10: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_10_B : ENTITY IS TRUE;

  end Dram_Addr_10_B;

  architecture Structure of Dram_Addr_10_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr10_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_10: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr10_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr10_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr10 	<= DramAddr10_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_9_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_9_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_9_B");

    port (DramAddr9: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_9_B : ENTITY IS TRUE;

  end Dram_Addr_9_B;

  architecture Structure of Dram_Addr_9_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr9_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_9: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr9_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr9_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr9 	<= DramAddr9_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_8_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_8_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_8_B");

    port (DramAddr8: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_8_B : ENTITY IS TRUE;

  end Dram_Addr_8_B;

  architecture Structure of Dram_Addr_8_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr8_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_8: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr8_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr8_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr8 	<= DramAddr8_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_7_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_7_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_7_B");

    port (DramAddr7: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_7_B : ENTITY IS TRUE;

  end Dram_Addr_7_B;

  architecture Structure of Dram_Addr_7_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr7_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_7: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr7_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr7_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr7 	<= DramAddr7_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_6_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_6_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_6_B");

    port (DramAddr6: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_6_B : ENTITY IS TRUE;

  end Dram_Addr_6_B;

  architecture Structure of Dram_Addr_6_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr6_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_6: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr6_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr6_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr6 	<= DramAddr6_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_5_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_5_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_5_B");

    port (DramAddr5: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_5_B : ENTITY IS TRUE;

  end Dram_Addr_5_B;

  architecture Structure of Dram_Addr_5_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr5_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_5: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr5_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr5_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr5 	<= DramAddr5_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_4_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_4_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_4_B");

    port (DramAddr4: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_4_B : ENTITY IS TRUE;

  end Dram_Addr_4_B;

  architecture Structure of Dram_Addr_4_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr4_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_4: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr4_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr4_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr4 	<= DramAddr4_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_3_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_3_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_3_B");

    port (DramAddr3: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_3_B : ENTITY IS TRUE;

  end Dram_Addr_3_B;

  architecture Structure of Dram_Addr_3_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr3_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_3: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr3_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr3_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr3 	<= DramAddr3_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_2_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_2_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_2_B");

    port (DramAddr2: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_2_B : ENTITY IS TRUE;

  end Dram_Addr_2_B;

  architecture Structure of Dram_Addr_2_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr2_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_2: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr2_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr2_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr2 	<= DramAddr2_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_1_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_1_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_1_B");

    port (DramAddr1: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_1_B : ENTITY IS TRUE;

  end Dram_Addr_1_B;

  architecture Structure of Dram_Addr_1_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr1_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_1: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr1_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr1_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr1 	<= DramAddr1_out;


    END PROCESS;

  end Structure;

-- entity Dram_Addr_0_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_Addr_0_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_Addr_0_B");

    port (DramAddr0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_Addr_0_B : ENTITY IS TRUE;

  end Dram_Addr_0_B;

  architecture Structure of Dram_Addr_0_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramAddr0_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Addr_pad_0: sapiobuf0003
      port map (I=>GNDI, PAD=>DramAddr0_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramAddr0_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramAddr0 	<= DramAddr0_out;


    END PROCESS;

  end Structure;

-- entity Dram_BA_1_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_BA_1_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_BA_1_B");

    port (DramBA1: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_BA_1_B : ENTITY IS TRUE;

  end Dram_BA_1_B;

  architecture Structure of Dram_BA_1_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramBA1_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_BA_pad_1: sapiobuf0003
      port map (I=>GNDI, PAD=>DramBA1_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramBA1_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramBA1 	<= DramBA1_out;


    END PROCESS;

  end Structure;

-- entity Dram_BA_0_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_BA_0_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_BA_0_B");

    port (DramBA0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_BA_0_B : ENTITY IS TRUE;

  end Dram_BA_0_B;

  architecture Structure of Dram_BA_0_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramBA0_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_BA_pad_0: sapiobuf0003
      port map (I=>GNDI, PAD=>DramBA0_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramBA0_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramBA0 	<= DramBA0_out;


    END PROCESS;

  end Structure;

-- entity Dram_n_WeB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_n_WeB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_n_WeB");

    port (DramnWe: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_n_WeB : ENTITY IS TRUE;

  end Dram_n_WeB;

  architecture Structure of Dram_n_WeB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramnWe_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_n_We_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramnWe_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramnWe_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramnWe 	<= DramnWe_out;


    END PROCESS;

  end Structure;

-- entity Dram_n_CasB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_n_CasB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_n_CasB");

    port (DramnCas: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_n_CasB : ENTITY IS TRUE;

  end Dram_n_CasB;

  architecture Structure of Dram_n_CasB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramnCas_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_n_Cas_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramnCas_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramnCas_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramnCas 	<= DramnCas_out;


    END PROCESS;

  end Structure;

-- entity Dram_n_RasB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_n_RasB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_n_RasB");

    port (DramnRas: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_n_RasB : ENTITY IS TRUE;

  end Dram_n_RasB;

  architecture Structure of Dram_n_RasB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramnRas_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_n_Ras_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramnRas_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramnRas_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramnRas 	<= DramnRas_out;


    END PROCESS;

  end Structure;

-- entity Dram_CKEB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_CKEB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_CKEB");

    port (DramCKE: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_CKEB : ENTITY IS TRUE;

  end Dram_CKEB;

  architecture Structure of Dram_CKEB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramCKE_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_CKE_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramCKE_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramCKE_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramCKE 	<= DramCKE_out;


    END PROCESS;

  end Structure;

-- entity Dram_ClkB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity Dram_ClkB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "Dram_ClkB");

    port (DramClk: out Std_logic);

    ATTRIBUTE Vital_Level0 OF Dram_ClkB : ENTITY IS TRUE;

  end Dram_ClkB;

  architecture Structure of Dram_ClkB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal DramClk_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0003
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    Dram_Clk_pad: sapiobuf0003
      port map (I=>GNDI, PAD=>DramClk_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (DramClk_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    DramClk 	<= DramClk_out;


    END PROCESS;

  end Structure;

-- entity slave_tx_oB
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity slave_tx_oB is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "slave_tx_oB");

    port (slavetxo: out Std_logic);

    ATTRIBUTE Vital_Level0 OF slave_tx_oB : ENTITY IS TRUE;

  end slave_tx_oB;

  architecture Structure of slave_tx_oB is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal slavetxo_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component gnd
      port (PWR0: out Std_logic);
    end component;
    component sapiobuf0002
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
  begin
    slave_tx_o_pad: sapiobuf0002
      port map (I=>GNDI, PAD=>slavetxo_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (slavetxo_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    slavetxo 	<= slavetxo_out;


    END PROCESS;

  end Structure;

-- entity LVDS_ck_0_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity LVDS_ck_0_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "LVDS_ck_0_B");

    port (LVDSck0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF LVDS_ck_0_B : ENTITY IS TRUE;

  end LVDS_ck_0_B;

  architecture Structure of LVDS_ck_0_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal LVDSck0_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component sapiobuf
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
    component gnd
      port (PWR0: out Std_logic);
    end component;
  begin
    LVDS_ck_pad_0: sapiobuf
      port map (I=>GNDI, PAD=>LVDSck0_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (LVDSck0_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    LVDSck0 	<= LVDSck0_out;


    END PROCESS;

  end Structure;

-- entity LVDS_Blue_0_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity LVDS_Blue_0_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "LVDS_Blue_0_B");

    port (LVDSBlue0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF LVDS_Blue_0_B : ENTITY IS TRUE;

  end LVDS_Blue_0_B;

  architecture Structure of LVDS_Blue_0_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal LVDSBlue0_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component sapiobuf
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
    component gnd
      port (PWR0: out Std_logic);
    end component;
  begin
    LVDS_Blue_pad_0: sapiobuf
      port map (I=>GNDI, PAD=>LVDSBlue0_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (LVDSBlue0_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    LVDSBlue0 	<= LVDSBlue0_out;


    END PROCESS;

  end Structure;

-- entity LVDS_Green_0_B
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity LVDS_Green_0_B is
    -- miscellaneous vital GENERICs
    GENERIC (
      TimingChecksOn	: boolean := TRUE;
      XOn           	: boolean := FALSE;
      MsgOn         	: boolean := TRUE;
      InstancePath  	: string := "LVDS_Green_0_B");

    port (LVDSGreen0: out Std_logic);

    ATTRIBUTE Vital_Level0 OF LVDS_Green_0_B : ENTITY IS TRUE;

  end LVDS_Green_0_B;

  architecture Structure of LVDS_Green_0_B is
    ATTRIBUTE Vital_Level0 OF Structure : ARCHITECTURE IS TRUE;

    signal LVDSGreen0_out 	: std_logic := 'X';

    signal GNDI: Std_logic;
    component sapiobuf
      port (I: in Std_logic; PAD: out Std_logic);
    end component;
    component gnd
      port (PWR0: out Std_logic);
    end component;
  begin
    LVDS_Green_pad_0: sapiobuf
      port map (I=>GNDI, PAD=>LVDSGreen0_out);
    DRIVEGND: gnd
      port map (PWR0=>GNDI);

    --  INPUT PATH DELAYs
    WireDelay : BLOCK
    BEGIN
    END BLOCK;

    VitalBehavior : PROCESS (LVDSGreen0_out)


    BEGIN

    IF (TimingChecksOn) THEN

    END IF;

    LVDSGreen0 	<= LVDSGreen0_out;


    END PROCESS;

  end Structure;

-- entity FleaFPGA_Ohm_A5
  library IEEE, vital2000, ECP5U;
  use IEEE.STD_LOGIC_1164.all;
  use vital2000.vital_timing.all;
  use ECP5U.COMPONENTS.ALL;

  entity FleaFPGA_Ohm_A5 is
    port (sys_clock: in Std_logic; sys_reset: in Std_logic; 
          n_led1: in Std_logic; LVDS_Red: out Std_logic_vector (0 to 0); 
          LVDS_Green: out Std_logic_vector (0 to 0); 
          LVDS_Blue: out Std_logic_vector (0 to 0); 
          LVDS_ck: out Std_logic_vector (0 to 0); slave_tx_o: out Std_logic; 
          slave_rx_i: in Std_logic; slave_cts_i: in Std_logic; 
          Dram_Clk: out Std_logic; Dram_CKE: out Std_logic; 
          Dram_n_Ras: out Std_logic; Dram_n_Cas: out Std_logic; 
          Dram_n_We: out Std_logic; Dram_BA: out Std_logic_vector (1 downto 0); 
          Dram_Addr: out Std_logic_vector (12 downto 0); 
          Dram_Data: in Std_logic_vector (15 downto 0); 
          Dram_n_cs: out Std_logic; Dram_DQMH: out Std_logic; 
          Dram_DQML: out Std_logic; GPIO_2: out Std_logic; 
          GPIO_3: in Std_logic; GPIO_4: in Std_logic; GPIO_5: in Std_logic; 
          GPIO_6: in Std_logic; GPIO_7: in Std_logic; GPIO_8: in Std_logic; 
          GPIO_9: in Std_logic; GPIO_10: in Std_logic; GPIO_11: in Std_logic; 
          GPIO_12: in Std_logic; GPIO_13: in Std_logic; GPIO_14: in Std_logic; 
          GPIO_15: in Std_logic; GPIO_16: in Std_logic; GPIO_17: in Std_logic; 
          GPIO_18: in Std_logic; GPIO_19: in Std_logic; GPIO_20: in Std_logic; 
          GPIO_21: in Std_logic; GPIO_22: in Std_logic; GPIO_23: in Std_logic; 
          GPIO_24: in Std_logic; GPIO_25: in Std_logic; GPIO_26: in Std_logic; 
          GPIO_27: in Std_logic; GPIO_IDSD: in Std_logic; 
          GPIO_IDSC: in Std_logic; mmc_dat1: in Std_logic; 
          mmc_dat2: in Std_logic; mmc_n_cs: out Std_logic; 
          mmc_clk: out Std_logic; mmc_mosi: out Std_logic; 
          mmc_miso: in Std_logic; PS2_enable: out Std_logic; 
          PS2_clk1: in Std_logic; PS2_data1: in Std_logic; 
          PS2_clk2: in Std_logic; PS2_data2: in Std_logic);



  end FleaFPGA_Ohm_A5;

  architecture Structure of FleaFPGA_Ohm_A5 is
    signal user_module1_mult1_un40_sum_ac0_4_1: Std_logic;
    signal user_module1_mult1_un40_sum_ac0_9_a0: Std_logic;
    signal user_module1_blink_led4_cry_6: Std_logic;
    signal user_module1_blink_led4: Std_logic;
    signal user_module1_un2_countlt11: Std_logic;
    signal user_module1_count_11: Std_logic;
    signal user_module1_duty_2: Std_logic;
    signal user_module1_duty_2_cry_0: Std_logic;
    signal user_module1_duty_2_S: Std_logic;
    signal user_module1_duty_1: Std_logic;
    signal user_module1_duty_2_2: Std_logic;
    signal user_module1_duty_2_1: Std_logic;
    signal sys_clock_c: Std_logic;
    signal user_module1_duty_2_cry_2: Std_logic;
    signal user_module1_duty_4: Std_logic;
    signal user_module1_duty_3: Std_logic;
    signal user_module1_duty_2_4: Std_logic;
    signal user_module1_duty_2_3: Std_logic;
    signal user_module1_duty_2_cry_4: Std_logic;
    signal user_module1_duty_6: Std_logic;
    signal user_module1_duty_5: Std_logic;
    signal user_module1_duty_2_6: Std_logic;
    signal user_module1_duty_2_5: Std_logic;
    signal user_module1_un5_count_cry_1_0_S1: Std_logic;
    signal user_module1_un2_countlto11: Std_logic;
    signal user_module1_mult1_un75_sum_cry_0: Std_logic;
    signal user_module1_mult1_un68_sum_6: Std_logic;
    signal user_module1_mult1_un68_sum_1: Std_logic;
    signal user_module1_un5_count_29: Std_logic;
    signal user_module1_mult1_un75_sum_1: Std_logic;
    signal user_module1_mult1_un75_sum_2: Std_logic;
    signal user_module1_mult1_un75_sum_cry_2: Std_logic;
    signal user_module1_mult1_un68_sum_3: Std_logic;
    signal user_module1_mult1_un68_sum_2: Std_logic;
    signal user_module1_mult1_un75_sum_3: Std_logic;
    signal user_module1_mult1_un75_sum_4: Std_logic;
    signal user_module1_mult1_un75_sum_cry_4: Std_logic;
    signal user_module1_mult1_un68_sum_5: Std_logic;
    signal user_module1_mult1_un68_sum_4: Std_logic;
    signal user_module1_mult1_un75_sum_5: Std_logic;
    signal user_module1_mult1_un75_sum_6: Std_logic;
    signal user_module1_count_0: Std_logic;
    signal user_module1_mult1_un89_sum_cry_0: Std_logic;
    signal user_module1_mult1_un82_sum_6: Std_logic;
    signal user_module1_mult1_un82_sum_1: Std_logic;
    signal user_module1_un5_count_31: Std_logic;
    signal user_module1_mult1_un89_sum_cry_2: Std_logic;
    signal user_module1_mult1_un82_sum_3: Std_logic;
    signal user_module1_mult1_un82_sum_2: Std_logic;
    signal user_module1_mult1_un89_sum_cry_4: Std_logic;
    signal user_module1_mult1_un82_sum_5: Std_logic;
    signal user_module1_mult1_un82_sum_4: Std_logic;
    signal user_module1_mult1_un89_sum_6: Std_logic;
    signal user_module1_un5_count_cry_0: Std_logic;
    signal user_module1_count_2: Std_logic;
    signal user_module1_count_1: Std_logic;
    signal user_module1_un5_count_cry_2: Std_logic;
    signal user_module1_count_4: Std_logic;
    signal user_module1_count_3: Std_logic;
    signal user_module1_un5_count_28: Std_logic;
    signal user_module1_un5_count_cry_4: Std_logic;
    signal user_module1_count_6: Std_logic;
    signal user_module1_count_5: Std_logic;
    signal user_module1_un5_count_27: Std_logic;
    signal user_module1_un5_count_cry_5_0_S1: Std_logic;
    signal user_module1_un5_count_cry_6: Std_logic;
    signal user_module1_count_8: Std_logic;
    signal user_module1_count_7: Std_logic;
    signal user_module1_un5_count_cry_7_0_S0: Std_logic;
    signal user_module1_un5_count_cry_7_0_S1: Std_logic;
    signal user_module1_un5_count_cry_8: Std_logic;
    signal user_module1_count_10: Std_logic;
    signal user_module1_count_9: Std_logic;
    signal user_module1_un5_count_cry_9_0_S1: Std_logic;
    signal user_module1_un5_count_cry_9_0_S0: Std_logic;
    signal user_module1_un5_count_cry_10: Std_logic;
    signal user_module1_un5_count_s_11_0_S0: Std_logic;
    signal user_module1_mult1_un68_sum_cry_0: Std_logic;
    signal user_module1_mult1_un61_sum_6: Std_logic;
    signal user_module1_mult1_un61_sum_1: Std_logic;
    signal user_module1_mult1_un68_sum_cry_2: Std_logic;
    signal user_module1_mult1_un61_sum_3: Std_logic;
    signal user_module1_mult1_un61_sum_2: Std_logic;
    signal user_module1_mult1_un68_sum_cry_4: Std_logic;
    signal user_module1_mult1_un61_sum_5: Std_logic;
    signal user_module1_mult1_un61_sum_4: Std_logic;
    signal user_module1_mult1_un82_sum_cry_0: Std_logic;
    signal user_module1_mult1_un82_sum_cry_2: Std_logic;
    signal user_module1_mult1_un82_sum_cry_4: Std_logic;
    signal user_module1_mult1_un47_sum_cry_0: Std_logic;
    signal user_module1_mult1_un40_sum_ac0_4_0: Std_logic;
    signal user_module1_mult1_un40_sum_axbxc1_0_0: Std_logic;
    signal user_module1_mult1_un47_sum_1: Std_logic;
    signal user_module1_mult1_un47_sum_2: Std_logic;
    signal user_module1_mult1_un47_sum_cry_2: Std_logic;
    signal user_module1_mult1_un40_sum_3: Std_logic;
    signal user_module1_mult1_un47_sum_cry_3_0_RNO: Std_logic;
    signal user_module1_mult1_un47_sum_3: Std_logic;
    signal user_module1_mult1_un47_sum_4: Std_logic;
    signal user_module1_mult1_un47_sum_cry_4: Std_logic;
    signal user_module1_mult1_un40_sum_axbxc5_0_1: Std_logic;
    signal user_module1_mult1_un40_sum_4: Std_logic;
    signal user_module1_mult1_un47_sum_5: Std_logic;
    signal user_module1_mult1_un47_sum_6: Std_logic;
    signal user_module1_mult1_un54_sum_cry_0: Std_logic;
    signal user_module1_mult1_un54_sum_1: Std_logic;
    signal user_module1_mult1_un54_sum_2: Std_logic;
    signal user_module1_mult1_un54_sum_cry_2: Std_logic;
    signal user_module1_mult1_un54_sum_3: Std_logic;
    signal user_module1_mult1_un54_sum_4: Std_logic;
    signal user_module1_mult1_un54_sum_cry_4: Std_logic;
    signal user_module1_mult1_un54_sum_5: Std_logic;
    signal user_module1_mult1_un54_sum_6: Std_logic;
    signal user_module1_mult1_un61_sum_cry_0: Std_logic;
    signal user_module1_mult1_un61_sum_cry_2: Std_logic;
    signal user_module1_mult1_un61_sum_cry_4: Std_logic;
    signal user_module1_blink_led4_cry_0: Std_logic;
    signal user_module1_blink_led4_cry_2: Std_logic;
    signal user_module1_blink_led4_cry_4: Std_logic;
    signal user_module1_count_3_2: Std_logic;
    signal user_module1_count_3_0: Std_logic;
    signal user_module1_count_3_7: Std_logic;
    signal user_module1_count_3_6: Std_logic;
    signal user_module1_count_3_11: Std_logic;
    signal user_module1_count_3_8: Std_logic;
    signal user_module1_duty_2_0: Std_logic;
    signal user_module1_un2_countlto8_1: Std_logic;
    signal user_module1_mult1_un40_sum_axbxc4_0_1: Std_logic;
    signal user_module1_if_N_7_mux: Std_logic;
    signal user_module1_N_7_mux: Std_logic;
    signal user_module1_un2_countlt8: Std_logic;
    signal user_module1_if_m3_e_1: Std_logic;
    signal user_module1_un2_countlto10_0: Std_logic;
    signal GPIO_2_c: Std_logic;
    signal VCCI: Std_logic;
    component LVDS_Red_0_B
      port (LVDSRed0: out Std_logic);
    end component;
    component sys_clockB
      port (PADDI: out Std_logic; sysclock: in Std_logic);
    end component;
    component PS2_enableB
      port (PS2enable: out Std_logic);
    end component;
    component mmc_mosiB
      port (mmcmosi: out Std_logic);
    end component;
    component mmc_clkB
      port (mmcclk: out Std_logic);
    end component;
    component mmc_n_csB
      port (mmcncs: out Std_logic);
    end component;
    component GPIO_2B
      port (IOLDO: in Std_logic; GPIO2: out Std_logic);
    end component;
    component GPIO_2_MGIOL
      port (IOLDO: out Std_logic; TXDATA0: in Std_logic; CLK: in Std_logic);
    end component;
    component Dram_DQMLB
      port (DramDQML: out Std_logic);
    end component;
    component Dram_DQMHB
      port (DramDQMH: out Std_logic);
    end component;
    component Dram_n_csB
      port (Dramncs: out Std_logic);
    end component;
    component Dram_Addr_12_B
      port (DramAddr12: out Std_logic);
    end component;
    component Dram_Addr_11_B
      port (DramAddr11: out Std_logic);
    end component;
    component Dram_Addr_10_B
      port (DramAddr10: out Std_logic);
    end component;
    component Dram_Addr_9_B
      port (DramAddr9: out Std_logic);
    end component;
    component Dram_Addr_8_B
      port (DramAddr8: out Std_logic);
    end component;
    component Dram_Addr_7_B
      port (DramAddr7: out Std_logic);
    end component;
    component Dram_Addr_6_B
      port (DramAddr6: out Std_logic);
    end component;
    component Dram_Addr_5_B
      port (DramAddr5: out Std_logic);
    end component;
    component Dram_Addr_4_B
      port (DramAddr4: out Std_logic);
    end component;
    component Dram_Addr_3_B
      port (DramAddr3: out Std_logic);
    end component;
    component Dram_Addr_2_B
      port (DramAddr2: out Std_logic);
    end component;
    component Dram_Addr_1_B
      port (DramAddr1: out Std_logic);
    end component;
    component Dram_Addr_0_B
      port (DramAddr0: out Std_logic);
    end component;
    component Dram_BA_1_B
      port (DramBA1: out Std_logic);
    end component;
    component Dram_BA_0_B
      port (DramBA0: out Std_logic);
    end component;
    component Dram_n_WeB
      port (DramnWe: out Std_logic);
    end component;
    component Dram_n_CasB
      port (DramnCas: out Std_logic);
    end component;
    component Dram_n_RasB
      port (DramnRas: out Std_logic);
    end component;
    component Dram_CKEB
      port (DramCKE: out Std_logic);
    end component;
    component Dram_ClkB
      port (DramClk: out Std_logic);
    end component;
    component slave_tx_oB
      port (slavetxo: out Std_logic);
    end component;
    component LVDS_ck_0_B
      port (LVDSck0: out Std_logic);
    end component;
    component LVDS_Blue_0_B
      port (LVDSBlue0: out Std_logic);
    end component;
    component LVDS_Green_0_B
      port (LVDSGreen0: out Std_logic);
    end component;
  begin
    user_module1_SLICE_0I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"b00f", INIT1_INITVAL=>X"5003")
      port map (M1=>'X', A1=>'1', B1=>'1', C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un40_sum_ac0_9_a0, 
                B0=>user_module1_mult1_un40_sum_ac0_4_1, C0=>'1', D0=>'1', 
                FCI=>user_module1_blink_led4_cry_6, M0=>'X', CE=>'X', CLK=>'X', 
                LSR=>'X', FCO=>open, F1=>user_module1_blink_led4, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_1I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"a60a")
      port map (M1=>'X', A1=>user_module1_duty_2, B1=>user_module1_count_11, 
                C1=>user_module1_un2_countlt11, D1=>'1', DI1=>'X', DI0=>'X', 
                A0=>'1', B0=>'1', C0=>'1', D0=>'1', FCI=>'X', M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_duty_2_cry_0, F1=>open, 
                Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_2I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a60a", 
                   REG1_SD=>"VHI", REG0_SD=>"VHI", CHECK_DI1=>TRUE, 
                   CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_duty_2_S, B1=>user_module1_count_11, 
                C1=>user_module1_un2_countlt11, D1=>'1', 
                DI1=>user_module1_duty_2_2, DI0=>user_module1_duty_2_1, 
                A0=>user_module1_duty_1, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_duty_2_cry_0, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>user_module1_duty_2_cry_2, 
                F1=>user_module1_duty_2_2, Q1=>user_module1_duty_2_S, 
                F0=>user_module1_duty_2_1, Q0=>user_module1_duty_1);
    user_module1_SLICE_3I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003", 
                   REG1_SD=>"VHI", REG0_SD=>"VHI", CHECK_DI1=>TRUE, 
                   CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_duty_4, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>user_module1_duty_2_4, DI0=>user_module1_duty_2_3, 
                A0=>user_module1_duty_3, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_duty_2_cry_2, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>user_module1_duty_2_cry_4, 
                F1=>user_module1_duty_2_4, Q1=>user_module1_duty_4, 
                F0=>user_module1_duty_2_3, Q0=>user_module1_duty_3);
    user_module1_SLICE_4I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003", 
                   REG1_SD=>"VHI", REG0_SD=>"VHI", CHECK_DI1=>TRUE, 
                   CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_duty_6, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>user_module1_duty_2_6, DI0=>user_module1_duty_2_5, 
                A0=>user_module1_duty_5, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_duty_2_cry_4, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>open, 
                F1=>user_module1_duty_2_6, Q1=>user_module1_duty_6, 
                F0=>user_module1_duty_2_5, Q0=>user_module1_duty_5);
    user_module1_SLICE_5I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"7008")
      port map (M1=>'X', A1=>user_module1_un2_countlto11, 
                B1=>user_module1_un5_count_cry_1_0_S1, C1=>'1', D1=>'1', 
                DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', D0=>'1', 
                FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_mult1_un75_sum_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_6I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"6005", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un68_sum_1, 
                B1=>user_module1_mult1_un68_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_29, 
                B0=>user_module1_mult1_un68_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un75_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un75_sum_cry_2, 
                F1=>user_module1_mult1_un75_sum_2, Q1=>open, 
                F0=>user_module1_mult1_un75_sum_1, Q0=>open);
    user_module1_SLICE_7I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"600a", INIT1_INITVAL=>X"600a")
      port map (M1=>'X', A1=>user_module1_mult1_un68_sum_6, 
                B1=>user_module1_mult1_un68_sum_3, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un68_sum_6, 
                B0=>user_module1_mult1_un68_sum_2, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un75_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un75_sum_cry_4, 
                F1=>user_module1_mult1_un75_sum_4, Q1=>open, 
                F0=>user_module1_mult1_un75_sum_3, Q0=>open);
    user_module1_SLICE_8I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"900a", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un68_sum_5, 
                B1=>user_module1_mult1_un68_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un68_sum_4, 
                B0=>user_module1_mult1_un68_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un75_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un75_sum_6, Q1=>open, 
                F0=>user_module1_mult1_un75_sum_5, Q0=>open);
    user_module1_SLICE_9I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"d002")
      port map (M1=>'X', A1=>user_module1_un2_countlto11, 
                B1=>user_module1_count_0, C1=>'1', D1=>'1', DI1=>'X', DI0=>'X', 
                A0=>'1', B0=>'1', C0=>'1', D0=>'1', FCI=>'X', M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un89_sum_cry_0, 
                F1=>open, Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_10I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"6005", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un82_sum_1, 
                B1=>user_module1_mult1_un82_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_31, 
                B0=>user_module1_mult1_un82_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un89_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un89_sum_cry_2, 
                F1=>open, Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_11I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"600a", INIT1_INITVAL=>X"600a")
      port map (M1=>'X', A1=>user_module1_mult1_un82_sum_6, 
                B1=>user_module1_mult1_un82_sum_3, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un82_sum_6, 
                B0=>user_module1_mult1_un82_sum_2, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un89_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un89_sum_cry_4, 
                F1=>open, Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_12I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"900a", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un82_sum_5, 
                B1=>user_module1_mult1_un82_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un82_sum_4, 
                B0=>user_module1_mult1_un82_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un89_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un89_sum_6, Q1=>open, F0=>open, 
                Q0=>open);
    user_module1_SLICE_13I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", INIT0_INITVAL=>X"500c", 
                   INIT1_INITVAL=>X"a003")
      port map (M1=>'X', A1=>user_module1_count_0, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', D0=>'1', 
                FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_un5_count_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_14I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003", 
                   REG0_SD=>"VHI", CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_count_2, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>'X', DI0=>user_module1_un5_count_31, 
                A0=>user_module1_count_1, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_un5_count_cry_0, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>user_module1_un5_count_cry_2, 
                F1=>user_module1_un5_count_cry_1_0_S1, Q1=>open, 
                F0=>user_module1_un5_count_31, Q0=>user_module1_count_1);
    user_module1_SLICE_15I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003", 
                   REG1_SD=>"VHI", REG0_SD=>"VHI", CHECK_DI1=>TRUE, 
                   CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_count_4, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>user_module1_un5_count_28, DI0=>user_module1_un5_count_29, 
                A0=>user_module1_count_3, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_un5_count_cry_2, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>user_module1_un5_count_cry_4, 
                F1=>user_module1_un5_count_28, Q1=>user_module1_count_4, 
                F0=>user_module1_un5_count_29, Q0=>user_module1_count_3);
    user_module1_SLICE_16I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003", 
                   REG0_SD=>"VHI", CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_count_6, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>'X', DI0=>user_module1_un5_count_27, 
                A0=>user_module1_count_5, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_un5_count_cry_4, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>user_module1_un5_count_cry_6, 
                F1=>user_module1_un5_count_cry_5_0_S1, Q1=>open, 
                F0=>user_module1_un5_count_27, Q0=>user_module1_count_5);
    user_module1_SLICE_17I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003")
      port map (M1=>'X', A1=>user_module1_count_8, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>'X', DI0=>'X', A0=>user_module1_count_7, B0=>'1', C0=>'1', 
                D0=>'1', FCI=>user_module1_un5_count_cry_6, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_un5_count_cry_8, 
                F1=>user_module1_un5_count_cry_7_0_S1, Q1=>open, 
                F0=>user_module1_un5_count_cry_7_0_S0, Q0=>open);
    user_module1_SLICE_18I: SCCU2C
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", CCU2_INJECT1_0=>"NO", 
                   CCU2_INJECT1_1=>"NO", GSR=>"DISABLED", SRMODE=>"ASYNC", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"a003", 
                   REG1_SD=>"VHI", REG0_SD=>"VHI", CHECK_DI1=>TRUE, 
                   CHECK_DI0=>TRUE)
      port map (M1=>'X', A1=>user_module1_count_10, B1=>'1', C1=>'1', D1=>'1', 
                DI1=>user_module1_un5_count_cry_9_0_S1, 
                DI0=>user_module1_un5_count_cry_9_0_S0, 
                A0=>user_module1_count_9, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_un5_count_cry_8, M0=>'X', CE=>'X', 
                CLK=>sys_clock_c, LSR=>'X', FCO=>user_module1_un5_count_cry_10, 
                F1=>user_module1_un5_count_cry_9_0_S1, 
                Q1=>user_module1_count_10, 
                F0=>user_module1_un5_count_cry_9_0_S0, 
                Q0=>user_module1_count_9);
    user_module1_SLICE_19I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"a003", INIT1_INITVAL=>X"5003")
      port map (M1=>'X', A1=>'1', B1=>'1', C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_count_11, B0=>'1', C0=>'1', D0=>'1', 
                FCI=>user_module1_un5_count_cry_10, M0=>'X', CE=>'X', CLK=>'X', 
                LSR=>'X', FCO=>open, F1=>open, Q1=>open, 
                F0=>user_module1_un5_count_s_11_0_S0, Q0=>open);
    user_module1_SLICE_20I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"500a")
      port map (M1=>'X', A1=>user_module1_un5_count_29, B1=>'1', C1=>'1', 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', 
                D0=>'1', FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_mult1_un68_sum_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_21I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"6005", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un61_sum_1, 
                B1=>user_module1_mult1_un61_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_28, 
                B0=>user_module1_mult1_un61_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un68_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un68_sum_cry_2, 
                F1=>user_module1_mult1_un68_sum_2, Q1=>open, 
                F0=>user_module1_mult1_un68_sum_1, Q0=>open);
    user_module1_SLICE_22I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"600a", INIT1_INITVAL=>X"600a")
      port map (M1=>'X', A1=>user_module1_mult1_un61_sum_6, 
                B1=>user_module1_mult1_un61_sum_3, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un61_sum_6, 
                B0=>user_module1_mult1_un61_sum_2, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un68_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un68_sum_cry_4, 
                F1=>user_module1_mult1_un68_sum_4, Q1=>open, 
                F0=>user_module1_mult1_un68_sum_3, Q0=>open);
    user_module1_SLICE_23I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"900a", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un61_sum_5, 
                B1=>user_module1_mult1_un61_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un61_sum_4, 
                B0=>user_module1_mult1_un61_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un68_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un68_sum_6, Q1=>open, 
                F0=>user_module1_mult1_un68_sum_5, Q0=>open);
    user_module1_SLICE_24I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"500a")
      port map (M1=>'X', A1=>user_module1_un5_count_31, B1=>'1', C1=>'1', 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', 
                D0=>'1', FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_mult1_un82_sum_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_25I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"7807", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un75_sum_1, 
                B1=>user_module1_mult1_un75_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_cry_1_0_S1, 
                B0=>user_module1_un2_countlto11, 
                C0=>user_module1_mult1_un75_sum_6, D0=>'1', 
                FCI=>user_module1_mult1_un82_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un82_sum_cry_2, 
                F1=>user_module1_mult1_un82_sum_2, Q1=>open, 
                F0=>user_module1_mult1_un82_sum_1, Q0=>open);
    user_module1_SLICE_26I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"600a", INIT1_INITVAL=>X"600a")
      port map (M1=>'X', A1=>user_module1_mult1_un75_sum_6, 
                B1=>user_module1_mult1_un75_sum_3, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un75_sum_6, 
                B0=>user_module1_mult1_un75_sum_2, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un82_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un82_sum_cry_4, 
                F1=>user_module1_mult1_un82_sum_4, Q1=>open, 
                F0=>user_module1_mult1_un82_sum_3, Q0=>open);
    user_module1_SLICE_27I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"900a", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un75_sum_5, 
                B1=>user_module1_mult1_un75_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un75_sum_4, 
                B0=>user_module1_mult1_un75_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un82_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un82_sum_6, Q1=>open, 
                F0=>user_module1_mult1_un82_sum_5, Q0=>open);
    user_module1_SLICE_28I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"7008")
      port map (M1=>'X', A1=>user_module1_un2_countlto11, 
                B1=>user_module1_un5_count_cry_5_0_S1, C1=>'1', D1=>'1', 
                DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', D0=>'1', 
                FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_mult1_un47_sum_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_29I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"7b77", INIT1_INITVAL=>X"d7dd")
      port map (M1=>'X', A1=>user_module1_un2_countlto11, 
                B1=>user_module1_mult1_un40_sum_axbxc1_0_0, 
                C1=>user_module1_mult1_un40_sum_ac0_9_a0, 
                D1=>user_module1_mult1_un40_sum_ac0_4_0, DI1=>'X', DI0=>'X', 
                A0=>user_module1_un5_count_cry_7_0_S0, 
                B0=>user_module1_un2_countlto11, 
                C0=>user_module1_mult1_un40_sum_ac0_9_a0, 
                D0=>user_module1_mult1_un40_sum_ac0_4_0, 
                FCI=>user_module1_mult1_un47_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un47_sum_cry_2, 
                F1=>user_module1_mult1_un47_sum_2, Q1=>open, 
                F0=>user_module1_mult1_un47_sum_1, Q0=>open);
    user_module1_SLICE_30I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"d20d", INIT1_INITVAL=>X"2d0d")
      port map (M1=>'X', A1=>user_module1_mult1_un40_sum_ac0_4_1, 
                B1=>user_module1_mult1_un40_sum_ac0_9_a0, 
                C1=>user_module1_mult1_un40_sum_3, D1=>'1', DI1=>'X', DI0=>'X', 
                A0=>user_module1_mult1_un40_sum_ac0_4_1, 
                B0=>user_module1_mult1_un40_sum_ac0_9_a0, 
                C0=>user_module1_mult1_un47_sum_cry_3_0_RNO, D0=>'1', 
                FCI=>user_module1_mult1_un47_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un47_sum_cry_4, 
                F1=>user_module1_mult1_un47_sum_4, Q1=>open, 
                F0=>user_module1_mult1_un47_sum_3, Q0=>open);
    user_module1_SLICE_31I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"a60a", INIT1_INITVAL=>X"de0a")
      port map (M1=>'X', A1=>user_module1_mult1_un40_sum_axbxc5_0_1, 
                B1=>user_module1_mult1_un40_sum_ac0_9_a0, 
                C1=>user_module1_mult1_un40_sum_ac0_4_1, D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un40_sum_4, 
                B0=>user_module1_mult1_un40_sum_ac0_4_1, 
                C0=>user_module1_mult1_un40_sum_ac0_9_a0, D0=>'1', 
                FCI=>user_module1_mult1_un47_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un47_sum_6, Q1=>open, 
                F0=>user_module1_mult1_un47_sum_5, Q0=>open);
    user_module1_SLICE_32I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"500a")
      port map (M1=>'X', A1=>user_module1_un5_count_27, B1=>'1', C1=>'1', 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', 
                D0=>'1', FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_mult1_un54_sum_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_33I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"7807", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un47_sum_1, 
                B1=>user_module1_mult1_un47_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_cry_5_0_S1, 
                B0=>user_module1_un2_countlto11, 
                C0=>user_module1_mult1_un47_sum_6, D0=>'1', 
                FCI=>user_module1_mult1_un54_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un54_sum_cry_2, 
                F1=>user_module1_mult1_un54_sum_2, Q1=>open, 
                F0=>user_module1_mult1_un54_sum_1, Q0=>open);
    user_module1_SLICE_34I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"600a", INIT1_INITVAL=>X"600a")
      port map (M1=>'X', A1=>user_module1_mult1_un47_sum_6, 
                B1=>user_module1_mult1_un47_sum_3, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un47_sum_6, 
                B0=>user_module1_mult1_un47_sum_2, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un54_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un54_sum_cry_4, 
                F1=>user_module1_mult1_un54_sum_4, Q1=>open, 
                F0=>user_module1_mult1_un54_sum_3, Q0=>open);
    user_module1_SLICE_35I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"900a", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un47_sum_5, 
                B1=>user_module1_mult1_un47_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un47_sum_4, 
                B0=>user_module1_mult1_un47_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un54_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un54_sum_6, Q1=>open, 
                F0=>user_module1_mult1_un54_sum_5, Q0=>open);
    user_module1_SLICE_36I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"500a")
      port map (M1=>'X', A1=>user_module1_un5_count_28, B1=>'1', C1=>'1', 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', 
                D0=>'1', FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_mult1_un61_sum_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_37I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"3c05", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un54_sum_1, 
                B1=>user_module1_mult1_un54_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_27, 
                B0=>user_module1_mult1_un54_sum_6, 
                C0=>user_module1_un5_count_27, D0=>'1', 
                FCI=>user_module1_mult1_un61_sum_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un61_sum_cry_2, 
                F1=>user_module1_mult1_un61_sum_2, Q1=>open, 
                F0=>user_module1_mult1_un61_sum_1, Q0=>open);
    user_module1_SLICE_38I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"600a", INIT1_INITVAL=>X"600a")
      port map (M1=>'X', A1=>user_module1_mult1_un54_sum_6, 
                B1=>user_module1_mult1_un54_sum_3, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un54_sum_6, 
                B0=>user_module1_mult1_un54_sum_2, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un61_sum_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_mult1_un61_sum_cry_4, 
                F1=>user_module1_mult1_un61_sum_4, Q1=>open, 
                F0=>user_module1_mult1_un61_sum_3, Q0=>open);
    user_module1_SLICE_39I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"900a", INIT1_INITVAL=>X"900a")
      port map (M1=>'X', A1=>user_module1_mult1_un54_sum_5, 
                B1=>user_module1_mult1_un54_sum_6, C1=>'1', D1=>'1', DI1=>'X', 
                DI0=>'X', A0=>user_module1_mult1_un54_sum_4, 
                B0=>user_module1_mult1_un54_sum_6, C0=>'1', D0=>'1', 
                FCI=>user_module1_mult1_un61_sum_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>open, 
                F1=>user_module1_mult1_un61_sum_6, Q1=>open, 
                F0=>user_module1_mult1_un61_sum_5, Q0=>open);
    user_module1_SLICE_40I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"5003", INIT1_INITVAL=>X"6905")
      port map (M1=>'X', A1=>user_module1_mult1_un89_sum_6, 
                B1=>user_module1_un2_countlto11, C1=>user_module1_duty_2, 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>'1', B0=>'1', C0=>'1', 
                D0=>'1', FCI=>'X', M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                FCO=>user_module1_blink_led4_cry_0, F1=>open, Q1=>open, 
                F0=>open, Q0=>open);
    user_module1_SLICE_41I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"3c05", INIT1_INITVAL=>X"3c05")
      port map (M1=>'X', A1=>user_module1_mult1_un75_sum_6, 
                B1=>user_module1_mult1_un75_sum_6, C1=>user_module1_duty_2_2, 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>user_module1_mult1_un82_sum_6, 
                B0=>user_module1_mult1_un82_sum_6, C0=>user_module1_duty_2_1, 
                D0=>'1', FCI=>user_module1_blink_led4_cry_0, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_blink_led4_cry_2, 
                F1=>open, Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_42I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"3c05", INIT1_INITVAL=>X"3c05")
      port map (M1=>'X', A1=>user_module1_mult1_un61_sum_6, 
                B1=>user_module1_mult1_un61_sum_6, C1=>user_module1_duty_2_4, 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>user_module1_mult1_un68_sum_6, 
                B0=>user_module1_mult1_un68_sum_6, C0=>user_module1_duty_2_3, 
                D0=>'1', FCI=>user_module1_blink_led4_cry_2, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_blink_led4_cry_4, 
                F1=>open, Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_43I: SCCU2C
      generic map (CCU2_INJECT1_0=>"NO", CCU2_INJECT1_1=>"NO", 
                   INIT0_INITVAL=>X"3c05", INIT1_INITVAL=>X"3c05")
      port map (M1=>'X', A1=>user_module1_mult1_un47_sum_6, 
                B1=>user_module1_mult1_un47_sum_6, C1=>user_module1_duty_2_6, 
                D1=>'1', DI1=>'X', DI0=>'X', A0=>user_module1_mult1_un54_sum_6, 
                B0=>user_module1_mult1_un54_sum_6, C0=>user_module1_duty_2_5, 
                D0=>'1', FCI=>user_module1_blink_led4_cry_4, M0=>'X', CE=>'X', 
                CLK=>'X', LSR=>'X', FCO=>user_module1_blink_led4_cry_6, 
                F1=>open, Q1=>open, F0=>open, Q0=>open);
    user_module1_SLICE_44I: SLOGICB
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", GSR=>"DISABLED", 
                   SRMODE=>"ASYNC", LUT0_INITVAL=>X"4444", 
                   LUT1_INITVAL=>X"A2A2", REG1_SD=>"VHI", REG0_SD=>"VHI", 
                   CHECK_DI1=>TRUE, CHECK_DI0=>TRUE)
      port map (M1=>'X', FXA=>'X', FXB=>'X', 
                A1=>user_module1_un5_count_cry_1_0_S1, 
                B1=>user_module1_count_11, C1=>user_module1_un2_countlt11, 
                D1=>'X', DI1=>user_module1_count_3_2, 
                DI0=>user_module1_count_3_0, A0=>user_module1_count_0, 
                B0=>user_module1_un2_countlto11, C0=>'X', D0=>'X', M0=>'X', 
                CE=>'X', CLK=>sys_clock_c, LSR=>'X', OFX1=>open, 
                F1=>user_module1_count_3_2, Q1=>user_module1_count_2, 
                OFX0=>open, F0=>user_module1_count_3_0, 
                Q0=>user_module1_count_0);
    user_module1_SLICE_45I: SLOGICB
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", GSR=>"DISABLED", 
                   SRMODE=>"ASYNC", LUT0_INITVAL=>X"A2A2", 
                   LUT1_INITVAL=>X"A2A2", REG1_SD=>"VHI", REG0_SD=>"VHI", 
                   CHECK_DI1=>TRUE, CHECK_DI0=>TRUE)
      port map (M1=>'X', FXA=>'X', FXB=>'X', 
                A1=>user_module1_un5_count_cry_7_0_S0, 
                B1=>user_module1_count_11, C1=>user_module1_un2_countlt11, 
                D1=>'X', DI1=>user_module1_count_3_7, 
                DI0=>user_module1_count_3_6, 
                A0=>user_module1_un5_count_cry_5_0_S1, 
                B0=>user_module1_count_11, C0=>user_module1_un2_countlt11, 
                D0=>'X', M0=>'X', CE=>'X', CLK=>sys_clock_c, LSR=>'X', 
                OFX1=>open, F1=>user_module1_count_3_7, 
                Q1=>user_module1_count_7, OFX0=>open, 
                F0=>user_module1_count_3_6, Q0=>user_module1_count_6);
    user_module1_SLICE_46I: SLOGICB
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", GSR=>"DISABLED", 
                   SRMODE=>"ASYNC", LUT0_INITVAL=>X"8888", 
                   LUT1_INITVAL=>X"8888", REG1_SD=>"VHI", REG0_SD=>"VHI", 
                   CHECK_DI1=>TRUE, CHECK_DI0=>TRUE)
      port map (M1=>'X', FXA=>'X', FXB=>'X', 
                A1=>user_module1_un5_count_s_11_0_S0, 
                B1=>user_module1_un2_countlto11, C1=>'X', D1=>'X', 
                DI1=>user_module1_count_3_11, DI0=>user_module1_count_3_8, 
                A0=>user_module1_un5_count_cry_7_0_S1, 
                B0=>user_module1_un2_countlto11, C0=>'X', D0=>'X', M0=>'X', 
                CE=>'X', CLK=>sys_clock_c, LSR=>'X', OFX1=>open, 
                F1=>user_module1_count_3_11, Q1=>user_module1_count_11, 
                OFX0=>open, F0=>user_module1_count_3_8, 
                Q0=>user_module1_count_8);
    user_module1_SLICE_47I: SLOGICB
      generic map (CLKMUX=>"SIG", CEMUX=>"VHI", GSR=>"DISABLED", 
                   SRMODE=>"ASYNC", LUT0_INITVAL=>X"9999", 
                   LUT1_INITVAL=>X"7F7F", REG0_SD=>"VHI", CHECK_DI0=>TRUE)
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>user_module1_count_6, 
                B1=>user_module1_count_7, C1=>user_module1_count_8, D1=>'X', 
                DI1=>'X', DI0=>user_module1_duty_2_0, A0=>user_module1_duty_2, 
                B0=>user_module1_un2_countlto11, C0=>'X', D0=>'X', M0=>'X', 
                CE=>'X', CLK=>sys_clock_c, LSR=>'X', OFX1=>open, 
                F1=>user_module1_un2_countlto8_1, Q1=>open, OFX0=>open, 
                F0=>user_module1_duty_2_0, Q0=>user_module1_duty_2);
    user_module1_SLICE_48I: SLOGICB
      generic map (LUT0_INITVAL=>X"3C88", LUT1_INITVAL=>X"6F60")
      port map (M1=>'X', FXA=>'X', FXB=>'X', 
                A1=>user_module1_un5_count_cry_7_0_S0, 
                B1=>user_module1_un5_count_s_11_0_S0, 
                C1=>user_module1_if_N_7_mux, 
                D1=>user_module1_mult1_un40_sum_axbxc4_0_1, DI1=>'X', DI0=>'X', 
                A0=>user_module1_un5_count_cry_9_0_S0, 
                B0=>user_module1_un5_count_cry_9_0_S1, 
                C0=>user_module1_un5_count_s_11_0_S0, 
                D0=>user_module1_un2_countlto11, M0=>'X', CE=>'X', CLK=>'X', 
                LSR=>'X', OFX1=>open, F1=>user_module1_mult1_un40_sum_4, 
                Q1=>open, OFX0=>open, 
                F0=>user_module1_mult1_un40_sum_axbxc4_0_1, Q0=>open);
    user_module1_SLICE_49I: SLOGICB
      generic map (LUT0_INITVAL=>X"EE0E", LUT1_INITVAL=>X"1E1E")
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>user_module1_N_7_mux, 
                B1=>user_module1_un5_count_cry_9_0_S0, 
                C1=>user_module1_un5_count_cry_9_0_S1, D1=>'X', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un5_count_cry_7_0_S0, 
                B0=>user_module1_un5_count_cry_7_0_S1, 
                C0=>user_module1_count_11, D0=>user_module1_un2_countlt11, 
                M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', OFX1=>open, 
                F1=>user_module1_mult1_un40_sum_3, Q1=>open, OFX0=>open, 
                F0=>user_module1_N_7_mux, Q0=>open);
    user_module1_SLICE_50I: SLOGICB
      generic map (LUT0_INITVAL=>X"000E", LUT1_INITVAL=>X"2222")
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>user_module1_count_11, 
                B1=>user_module1_un2_countlt11, C1=>'X', D1=>'X', DI1=>'X', 
                DI0=>'X', A0=>user_module1_un2_countlto8_1, 
                B0=>user_module1_un2_countlt8, C0=>user_module1_count_10, 
                D0=>user_module1_count_9, M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', 
                OFX1=>open, F1=>user_module1_if_m3_e_1, Q1=>open, OFX0=>open, 
                F0=>user_module1_un2_countlt11, Q0=>open);
    user_module1_SLICE_51I: SLOGICB
      generic map (LUT0_INITVAL=>X"1EF0", LUT1_INITVAL=>X"FD55")
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>user_module1_count_11, 
                B1=>user_module1_un2_countlt8, 
                C1=>user_module1_un2_countlto8_1, 
                D1=>user_module1_un2_countlto10_0, DI1=>'X', DI0=>'X', 
                A0=>user_module1_un5_count_cry_7_0_S0, 
                B0=>user_module1_un5_count_cry_7_0_S1, 
                C0=>user_module1_un5_count_cry_9_0_S0, 
                D0=>user_module1_un2_countlto11, M0=>'X', CE=>'X', CLK=>'X', 
                LSR=>'X', OFX1=>open, F1=>user_module1_un2_countlto11, 
                Q1=>open, OFX0=>open, 
                F0=>user_module1_mult1_un47_sum_cry_3_0_RNO, Q0=>open);
    user_module1_SLICE_52I: SLOGICB
      generic map (LUT0_INITVAL=>X"77F7", LUT1_INITVAL=>X"8808")
      port map (M1=>'X', FXA=>'X', FXB=>'X', 
                A1=>user_module1_un5_count_cry_9_0_S1, 
                B1=>user_module1_un5_count_s_11_0_S0, 
                C1=>user_module1_count_11, D1=>user_module1_un2_countlt11, 
                DI1=>'X', DI0=>'X', A0=>user_module1_un5_count_cry_9_0_S1, 
                B0=>user_module1_un5_count_s_11_0_S0, 
                C0=>user_module1_count_11, D0=>user_module1_un2_countlt11, 
                M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', OFX1=>open, 
                F1=>user_module1_mult1_un40_sum_ac0_4_1, Q1=>open, OFX0=>open, 
                F0=>user_module1_mult1_un40_sum_axbxc5_0_1, Q0=>open);
    user_module1_SLICE_53I: SLOGICB
      generic map (LUT0_INITVAL=>X"0001", LUT1_INITVAL=>X"1111")
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>user_module1_count_9, 
                B1=>user_module1_count_10, C1=>'X', D1=>'X', DI1=>'X', 
                DI0=>'X', A0=>user_module1_count_2, B0=>user_module1_count_3, 
                C0=>user_module1_count_4, D0=>user_module1_count_5, M0=>'X', 
                CE=>'X', CLK=>'X', LSR=>'X', OFX1=>open, 
                F1=>user_module1_un2_countlto10_0, Q1=>open, OFX0=>open, 
                F0=>user_module1_un2_countlt8, Q0=>open);
    user_module1_SLICE_54I: SLOGICB
      generic map (LUT0_INITVAL=>X"0010", LUT1_INITVAL=>X"8888")
      port map (M1=>'X', FXA=>'X', FXB=>'X', 
                A1=>user_module1_un5_count_cry_9_0_S1, 
                B1=>user_module1_un5_count_s_11_0_S0, C1=>'X', D1=>'X', 
                DI1=>'X', DI0=>'X', A0=>user_module1_un5_count_cry_7_0_S1, 
                B0=>user_module1_un5_count_cry_9_0_S0, 
                C0=>user_module1_un5_count_cry_9_0_S1, 
                D0=>user_module1_if_m3_e_1, M0=>'X', CE=>'X', CLK=>'X', 
                LSR=>'X', OFX1=>open, F1=>user_module1_mult1_un40_sum_ac0_4_0, 
                Q1=>open, OFX0=>open, F0=>user_module1_if_N_7_mux, Q0=>open);
    user_module1_SLICE_55I: SLOGICB
      generic map (LUT0_INITVAL=>X"0101")
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>'X', B1=>'X', C1=>'X', 
                D1=>'X', DI1=>'X', DI0=>'X', 
                A0=>user_module1_un5_count_cry_7_0_S0, 
                B0=>user_module1_un5_count_cry_7_0_S1, 
                C0=>user_module1_un5_count_cry_9_0_S0, D0=>'X', M0=>'X', 
                CE=>'X', CLK=>'X', LSR=>'X', OFX1=>open, F1=>open, Q1=>open, 
                OFX0=>open, F0=>user_module1_mult1_un40_sum_ac0_9_a0, Q0=>open);
    user_module1_SLICE_56I: SLOGICB
      generic map (LUT0_INITVAL=>X"9999")
      port map (M1=>'X', FXA=>'X', FXB=>'X', A1=>'X', B1=>'X', C1=>'X', 
                D1=>'X', DI1=>'X', DI0=>'X', 
                A0=>user_module1_un5_count_cry_7_0_S0, 
                B0=>user_module1_un5_count_cry_7_0_S1, C0=>'X', D0=>'X', 
                M0=>'X', CE=>'X', CLK=>'X', LSR=>'X', OFX1=>open, F1=>open, 
                Q1=>open, OFX0=>open, 
                F0=>user_module1_mult1_un40_sum_axbxc1_0_0, Q0=>open);
    LVDS_Red_0_I: LVDS_Red_0_B
      port map (LVDSRed0=>LVDS_Red(0));
    sys_clockI: sys_clockB
      port map (PADDI=>sys_clock_c, sysclock=>sys_clock);
    PS2_enableI: PS2_enableB
      port map (PS2enable=>PS2_enable);
    mmc_mosiI: mmc_mosiB
      port map (mmcmosi=>mmc_mosi);
    mmc_clkI: mmc_clkB
      port map (mmcclk=>mmc_clk);
    mmc_n_csI: mmc_n_csB
      port map (mmcncs=>mmc_n_cs);
    GPIO_2I: GPIO_2B
      port map (IOLDO=>GPIO_2_c, GPIO2=>GPIO_2);
    GPIO_2_MGIOLI: GPIO_2_MGIOL
      port map (IOLDO=>GPIO_2_c, TXDATA0=>user_module1_blink_led4, 
                CLK=>sys_clock_c);
    Dram_DQMLI: Dram_DQMLB
      port map (DramDQML=>Dram_DQML);
    Dram_DQMHI: Dram_DQMHB
      port map (DramDQMH=>Dram_DQMH);
    Dram_n_csI: Dram_n_csB
      port map (Dramncs=>Dram_n_cs);
    Dram_Addr_12_I: Dram_Addr_12_B
      port map (DramAddr12=>Dram_Addr(12));
    Dram_Addr_11_I: Dram_Addr_11_B
      port map (DramAddr11=>Dram_Addr(11));
    Dram_Addr_10_I: Dram_Addr_10_B
      port map (DramAddr10=>Dram_Addr(10));
    Dram_Addr_9_I: Dram_Addr_9_B
      port map (DramAddr9=>Dram_Addr(9));
    Dram_Addr_8_I: Dram_Addr_8_B
      port map (DramAddr8=>Dram_Addr(8));
    Dram_Addr_7_I: Dram_Addr_7_B
      port map (DramAddr7=>Dram_Addr(7));
    Dram_Addr_6_I: Dram_Addr_6_B
      port map (DramAddr6=>Dram_Addr(6));
    Dram_Addr_5_I: Dram_Addr_5_B
      port map (DramAddr5=>Dram_Addr(5));
    Dram_Addr_4_I: Dram_Addr_4_B
      port map (DramAddr4=>Dram_Addr(4));
    Dram_Addr_3_I: Dram_Addr_3_B
      port map (DramAddr3=>Dram_Addr(3));
    Dram_Addr_2_I: Dram_Addr_2_B
      port map (DramAddr2=>Dram_Addr(2));
    Dram_Addr_1_I: Dram_Addr_1_B
      port map (DramAddr1=>Dram_Addr(1));
    Dram_Addr_0_I: Dram_Addr_0_B
      port map (DramAddr0=>Dram_Addr(0));
    Dram_BA_1_I: Dram_BA_1_B
      port map (DramBA1=>Dram_BA(1));
    Dram_BA_0_I: Dram_BA_0_B
      port map (DramBA0=>Dram_BA(0));
    Dram_n_WeI: Dram_n_WeB
      port map (DramnWe=>Dram_n_We);
    Dram_n_CasI: Dram_n_CasB
      port map (DramnCas=>Dram_n_Cas);
    Dram_n_RasI: Dram_n_RasB
      port map (DramnRas=>Dram_n_Ras);
    Dram_CKEI: Dram_CKEB
      port map (DramCKE=>Dram_CKE);
    Dram_ClkI: Dram_ClkB
      port map (DramClk=>Dram_Clk);
    slave_tx_oI: slave_tx_oB
      port map (slavetxo=>slave_tx_o);
    LVDS_ck_0_I: LVDS_ck_0_B
      port map (LVDSck0=>LVDS_ck(0));
    LVDS_Blue_0_I: LVDS_Blue_0_B
      port map (LVDSBlue0=>LVDS_Blue(0));
    LVDS_Green_0_I: LVDS_Green_0_B
      port map (LVDSGreen0=>LVDS_Green(0));
    VHI_INST: VHI
      port map (Z=>VCCI);
    PUR_INST: PUR
      port map (PUR=>VCCI);
    GSR_INST: GSR
      port map (GSR=>VCCI);
  end Structure;



  library IEEE, vital2000, ECP5U;
  configuration Structure_CON of FleaFPGA_Ohm_A5 is
    for Structure
    end for;
  end Structure_CON;


