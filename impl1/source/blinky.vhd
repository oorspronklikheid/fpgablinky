----------------------------------------------------------------------------------
-- ********* FleaFPGA Ohm 'Blinky LED' example ***********
-- Very simple example of how to create a custom logic module using HDL
-- Following VHDL code describes an up-counter with a count range of 12.5 million
-- Counter value is incremented by the external system clock (which is 25MHz on 
-- the fleaFPGA Ohm) the 'STAT' LED on FleaFPGA will be toggled, every time the 
-- counter value reaches 12.5 million and reset back to zero.
--
-- Please Note: This source code is provided as-is and is only intended to 
-- illustrate how to implement a simple VHDL project using FleaFPGA Ohm.
--
-- Creation Date: 25th October 2017
-- Author: Valentin Angelovski
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity blinky is

  port(
		clk: in  STD_LOGIC;
		blink_LED: BUFFER  STD_LOGIC;
		inputty: BUFFER  STD_LOGIC);

END blinky;  
  
  
ARCHITECTURE behavior OF blinky IS

BEGIN
   PROCESS(clk)
      VARIABLE count :   INTEGER RANGE 0 TO 50_000_000;
	  VARIABLE duty  :	 INTEGER RANGE 0 TO 100 ;
  	  VARIABLE compare : INTEGER RANGE 0 to 50_000_000;
   BEGIN
   
   
      IF(clk'EVENT AND clk = '1') THEN
	  
	  IF(inputty = '1')
	  THEN
		compare := 25_000 ;
	  ELSE
		compare := 2_500	 ;
	  END IF ;
		--blink_LED <= NOT blink_LED;
		   IF(count < compare) THEN
		 
            count := count + 1;
         ELSE
		 
		    count := 0;
            --duty := duty + 10 ;
			duty := 50 ;
			if (duty  > 100 ) 
			THEN
			duty :=0 ;
			END IF ;
         END IF;
		 if( 100* count/compare  > duty)
		 THEN
			--inputty <= '0';
			blink_LED <='1';
		 ELSE 
			--inputty <= '1';
			blink_LED <= '0';
		 END IF;
        		 
      END IF;
   END PROCESS; 
 
 
end architecture;