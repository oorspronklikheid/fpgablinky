setenv SIM_WORKING_FOLDER .
set newDesign 0
if {![file exists "C:/Users/password/Documents/Projects/fpga/A123/A123.adf"]} { 
	design create A123 "C:/Users/password/Documents/Projects/fpga"
  set newDesign 1
}
design open "C:/Users/password/Documents/Projects/fpga/A123"
cd "C:/Users/password/Documents/Projects/fpga"
designverincludedir -clear
designverlibrarysim -PL -clear
designverlibrarysim -L -clear
designverlibrarysim -PL pmi_work
designverlibrarysim ovi_ecp5u
designverdefinemacro -clear
if {$newDesign == 0} { 
  removefile -Y -D *
}
addfile "C:/Users/password/Documents/Projects/fpga/impl1/source/blinky.vhd"
addfile "C:/Users/password/Documents/Projects/fpga/impl1/source/FleaFPGA_Ohm_A5_Top.vhd"
vlib "C:/Users/password/Documents/Projects/fpga/A123/work"
set worklib work
adel -all
vcom -dbg -work work "C:/Users/password/Documents/Projects/fpga/impl1/source/blinky.vhd"
vcom -dbg -work work "C:/Users/password/Documents/Projects/fpga/impl1/source/FleaFPGA_Ohm_A5_Top.vhd"
entity FleaFPGA_Ohm_A5
vsim  +access +r FleaFPGA_Ohm_A5   -PL pmi_work -L ovi_ecp5u
add wave *
run 1000ns
